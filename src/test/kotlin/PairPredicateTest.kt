import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class PairPredicateTest {

    /**
     * Le Kotlin way (concis pragmatique, simple et efficace) :
     * Tout le code superflu a été supprimé !!!
     */
    @Test
    fun `should work using conversion From Single Abstract Method with Lambda`() {
        val pairPredicate = IntPredicate { it % 2 == 0 }

        Assertions.assertTrue(pairPredicate.accept(2))
        Assertions.assertTrue(pairPredicate.accept(134))
        Assertions.assertTrue(pairPredicate.accept(2434338))
    }

    /**
     * Pour se faire une idée de ce qui est superflu :
     * Le very classical way pour l'implémenter :
     * */
    @Test
    fun `should work using classical class and instanciation`() {
        class PairPredicate : IntPredicate {
            override fun accept(i: Int): Boolean {
                return i % 2 == 0
            }
        }

        val pairPredicate = PairPredicate()

        Assertions.assertTrue(pairPredicate.accept(2))
        Assertions.assertTrue(pairPredicate.accept(134))
        Assertions.assertTrue(pairPredicate.accept(2434338))
    }

    /**
     * une version one shot avec les expressions d'objet
     * plus élégant mais pas forcément plus léger que le classical
     */
    @Test
    fun `should work using objet expression`() {
        val pairPredicate = object : IntPredicate {
            override fun accept(i: Int): Boolean {
                return i % 2 == 0
            }
        }

        Assertions.assertTrue(pairPredicate.accept(2))
        Assertions.assertTrue(pairPredicate.accept(134))
        Assertions.assertTrue(pairPredicate.accept(2434338))
    }
}